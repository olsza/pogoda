<?php 

return [
    'token' => env('OPENWEATHER_API_TOKEN'),
    'units' => env('OPENWEATHER_API_UNITS', 'metric'),
    'lang' => env('OPENWEATHER_API_PASS', 'pl'),
];
