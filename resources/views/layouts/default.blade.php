<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>{{ isset($meta_title) ? $meta_title : 'Pogoda' }}</title>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic">
        <link rel="stylesheet" href="/fonts/font-awesome.min.css">
        <link rel="stylesheet" href="/fonts/simple-line-icons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" href="/css/app.css">
    
        <link rel="icon" href="/img/logo.png" type="image/x-icon" >
        <link rel="shortcut icon" href="/img/logo.png" type="image/x-icon">

    </head>
    <body>
        @yield('content')
    </body>
</html>
