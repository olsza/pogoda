<div class="mx-auto features-icons-item mb-5 mb-lg-0 mb-lg-3">
<div class="d-flex features-icons-icon"><span class="m-auto iconsize">{{ $information['temp_city'] ?? '-'}}<small>&#x2103</small></span></div>
    <h3>Temperatura</h3>
    <p class="lead mb-0">wschód o {{ $information['sun_start'] ?? '-'}}, zachód o {{ $information['sun_end'] ?? '-'}}</p>
</div>
