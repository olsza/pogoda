<div class="mx-auto features-icons-item mb-5 mb-lg-0 mb-lg-3">
    <div class="d-flex features-icons-icon"><i class="fa fa-arrow-up m-auto iconsize" style="transform: rotate({{ $information['wind_deg'] ?? 0 }}deg);"></i></div>
    <h3>Wiatr</h3>
    <p class="lead mb-0">{{ $information['wind_city'] ?? '-'}} m/s</p>
</div>
