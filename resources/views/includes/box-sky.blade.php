<div class="mx-auto features-icons-item mb-5 mb-lg-0 mb-lg-3">
    <div class="d-flex features-icons-icon">
        <img src="{{ $information['icon_link'] }}" alt="{{ $information['weather_info'] ?? ''}}" class="m-auto iconweather">
    </div>
    <h3>Niebo</h3>
    <p class="lead mb-0">{{ $information['weather_info'] ?? ''}}</p>
</div>
