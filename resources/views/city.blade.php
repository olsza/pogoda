@extends('layouts.default')

@section('content')

<header class="masthead text-white text-center" style="">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h1 class="mb-1">Pogoda dla&nbsp;<span class="cityname">{{ $information['name_city'] ?? 'brak danych'}}</span></h1>
                <p class="update">Aktulizacja:&nbsp;<span class="dateupdate">{{ $information['date_update'] ?? 'bd'}}</span></p>
            </div>
        </div>
    </div>
</header>
<section class="features-icons bg-light text-center">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-4 order-3 order-lg-1">
                @include('includes.box-wind')
            </div>
            <div class="col-lg-4 order-1 order-lg-2">
                @include('includes.box-temperature')
            </div>
            <div class="col-sm-6 col-lg-4 order-3">
                @include('includes.box-sky')
            </div>
        </div>
    </div>
</section>
<footer class="footer bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 my-auto h-100 text-center text-lg-left">
                <p class="text-muted small mb-4 mb-lg-0">&copy; Olsza 2019. All Rights Reserved.</p>
            </div>
            <div class="col-lg-6 my-auto h-100 text-center text-lg-right">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item"></li>
                    <li class="list-inline-item"><a href="https://twitter.com/czlowiek_it" target="_blank"><i class="fa fa-twitter fa-2x fa-fw"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
@endsection
