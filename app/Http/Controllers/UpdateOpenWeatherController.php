<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OpenweathermapCity as City;
use App\OpenweathermapData as DataWeather;
use App\Http\Controllers\WeatherController;
use Illuminate\Support\Carbon;
use App\Api\OpenWeather;
use App\OpenweathermapData;
use SebastianBergmann\Type\TypeName;

class UpdateOpenWeatherController extends Controller
{

    const DEFAULT_TIME_CHECKING_MINUTES = 60;

    public function updateWithApi($nameCity)
    {
        $nameCity = $nameCity ?? 'Warszawa';

        if($this->is_updating())
        {
            $owd = new OpenweathermapData();
            $data = $this->downloadWithApiByNamaCity($nameCity);
            if ($data) {
                $jsonObject = json_decode($data);

                $owd->data = $data;
                $owd->city_id = $jsonObject->id ?? 0;
                $owd->code = $jsonObject->cod;

                $owd->save();
            }
        }
    }

    protected function is_updating($min = self::DEFAULT_TIME_CHECKING_MINUTES)
    {
        $owd = new OpenweathermapData();
        $endDate = $owd->orderBy('updated_at', 'DESC')->first();

        // 10sekund odjętę dla zapisanych opóźnien w BD
        return Carbon::parse($endDate->updated_at)->addMinutes($min)->subSeconds(10) < Carbon::now();
    }

    protected function downloadWithApiByNamaCity($nameCity)
    {
        $api = new OpenWeather();
        $cityId = $this->findIdForCityName($nameCity);

        if ($cityId) {
            return $api->getWeatherByCityId($cityId);
        }

        return null;
    }

    protected function findIdForCityName($nameCity)
    {
        $cityBd = City::where('name', $nameCity)->first();
        
        return $cityBd->id ?? null;
    }
}
