<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OpenweathermapCity;
use Illuminate\Support\Carbon;

class WeatherController extends Controller
{
    protected $dataCity;
    protected $nameCity;
    protected $dateUpdate;
    protected $timeUpdateInApi;
    protected $fullData;
    protected $temperature;
    protected $windspeed;
    protected $winddeg;
    protected $startSun;
    protected $endSun;
    protected $infoWeatherDesc;
    protected $timezone = 0;
    protected $icon;

    public function __construct($nameCity = 'Warszawa')
    {
        $this->nameCity = $this->getNameCity() ?? $nameCity;
    }

    public function getWeatherForCity($nameCity)
    {
        return [
            'name_city' => $this->getNameCityByData($nameCity),
            'temp_city' => $this->getTemperature(),
            'wind_city' => $this->getWindSpeed(),
            'wind_deg' => $this->getWindDeg(),
            'date_update' => $this->getDateUpdate(),
            'weather_info' => $this->getInfoWeatherDesc(),
            'sun_start' => $this->getStartSun(),
            'sun_end' => $this->getEndSun(),
            'icon_link' => $this->getIconUrl(),
        ];
    }

    public function getDateUpdate()
    {
        $time = new Carbon();

        return $time->parse($this->dateUpdate)->format('Y-m-d H:i');
    }

    public function getNameCity()
    {
        return $this->nameCity;
    }

    public function getFullData()
    {
        return $this->fullData;
    }

    public function getTemperature()
    {
        if(is_null($this->temperature))
        {
            $this->setTemperature();
        }

        return round($this->temperature, 1);
    }

    public function getWindSpeed()
    {
        if(is_null($this->windspeed))
        {
            $this->setWindSpeed();
        }

        return $this->windspeed;
    }

    public function getWindDeg()
    {
        if(is_null($this->winddeg))
        {
            $this->setWindDeg();
        }

        return $this->winddeg;
    }

    public function getStartSun($format = 'G:i')
    {
        if(is_null($this->startSun))
        {
            $this->setStartSun();
        }
        $time = new Carbon($this->startSun);

        return $time->format($format);
    }

    public function getEndSun($format = 'G:i')
    {
        if(is_null($this->endSun))
        {
            $this->setEndSun();
        }

        $time = new Carbon($this->endSun);

        return $time->format($format);
    }

    public function getInfoWeatherDesc()
    {
        if(is_null($this->infoWeatherDesc))
        {
            $this->setInfoWeatherDesc();
        }

        return $this->infoWeatherDesc;
    }

    public function getIdCity()
    {
        if (!is_null($this->dataCity)) {
            $actualData = $this->getActualData();

            if ($actualData) {
                return json_decode($actualData->data)->id;
            }
        }
        
        return null;
    }

    public function getIconUrl()
    {
        if (is_null($this->icon)) {
            $this->setIconLink();
        }

        return $this->icon;
    }

    protected function getNameCityByData($nameCity)
    {
        $this->setNameCity($this->readDataCity($nameCity)->name ?? null);

        return $this->getNameCity();
    }

    protected function setNameCity($nameCity)
    {
        $this->nameCity = $nameCity;
        $this->setDataCity($nameCity);
    }

    protected function setDataCity($nameCity)
    {
        $this->dataCity = OpenweathermapCity::where('name', $nameCity)->first();
        $this->setFullData();
        $this->setDateUpdate();
    }

    protected function getDataCity()
    {
        return $this->dataCity;
    }

    public function readDataCity($nameCity)
    {
        if(!$this->getDataCity() || $this->getNameCity() != $nameCity)
        {
            $this->setDataCity($nameCity);
        }

        return $this->getDataCity();
    }

    protected function getActualData()
    {
        return $this->getDataCity()->actualData();
    }

    protected function setDateUpdate()
    {
        if(!is_null($this->dataCity) && $this->getActualData())
        {
            $this->dateUpdate = $this->getActualData()->updated_at;
        }
    }

    protected function setFullData()
    {
        if(!is_null($this->dataCity))
        {
            $actualData = $this->getActualData();

            if($actualData)
            {
                $this->fullData = json_decode($actualData->data);
                $this->timezone = $this->fullData->timezone ?? 0;
            }
        }
    }

    protected function setTemperature()
    {
        if(!is_null($this->dataCity) && $this->getFullData())
        {
            $this->temperature = $this->getFullData()->main->temp ?? null;
        }
    }

    protected function setWindSpeed()
    {
        if(!is_null($this->dataCity) && $this->getFullData())
        {
            $this->windspeed = $this->getFullData()->wind->speed ?? null;
        }
    }

    protected function setWindDeg()
    {
        if(!is_null($this->dataCity) && $this->getFullData())
        {
            $this->winddeg = $this->getFullData()->wind->deg ?? null;
        }
    }

    protected function setInfoWeatherDesc()
    {
        if(!is_null($this->dataCity) && $this->getFullData())
        {
            $this->infoWeatherDesc = $this->getFullData()->weather[0]->description ?? null;
        }
    }

    protected function setStartSun()
    {
        if(!is_null($this->dataCity) && $this->getFullData())
        {
            $this->startSun = $this->getFullData()->sys->sunrise + $this->timezone;
        }
    }

    protected function setEndSun()
    {
        if(!is_null($this->dataCity) && $this->getFullData())
        {
            $this->endSun = $this->getFullData()->sys->sunset + $this->timezone;
        }
    }

    protected function setIconLink()
    {
        if(!is_null($this->dataCity) && $this->getFullData())
        {
            $icon = $this->getFullData()->weather[0]->icon ?? null;

            $this->icon = "http://openweathermap.org/img/wn/$icon@2x.png";
        }
    }
}