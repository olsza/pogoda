<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\WeatherController;

class ViewController extends WeatherController
{
    public function viewWeatherForCity()
    {
        $information = $this->getWeatherForCity('warszawa');

        return View('city', compact('information'));
    }
}
