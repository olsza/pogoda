<?php

namespace App\Api;

use Illuminate\Http\Request;
use GuzzleHttp;
use Illuminate\Support\Facades\Cache;

class OpenWeather
{
    private
        $appid, // token
        $units, // units, metric
        $lang,
        $url;
 
    const CACHE_EXPIRATION = 60;

    public function __construct()
    {
        $this->appid = config('openweather.token');
        $this->units = config('openweather.units');
        $this->lang = config('openweather.lang');
        $this->url = 'https://api.openweathermap.org/data/2.5/';
    }

    public function getWeatherByCityName($name)
    {
        return $this->send('weather', "q=$name");
    }

    public function getWeatherByCityId($id)
    {
        return $this->send('weather', "id=$id");
    }

    public function getWeatherByZip($zip)
    {
        return $this->send('weather', "zip=$zip");
    }

    protected function getFullRequest($question = '', $addParametrs = null)
    {
        return $this->url . "$question?units=$this->units&mode=json&lang=$this->lang&appid=$this->appid" . ($addParametrs ? "&$addParametrs" : '');
    }

    protected function send($question = null, $parametrs = null)
    {
        $url = strtolower($this->getFullRequest($question, $parametrs));

        $cacheUrl = md5($url);
        if (Cache::has($cacheUrl)) {
            return Cache::get($cacheUrl);
        }

        try {
            $guzzle = new GuzzleHttp\Client();
            $response = $guzzle->request('GET', $url);
            $result = $response->getBody()->getContents();
            Cache::put($cacheUrl, $result, self::CACHE_EXPIRATION);
        } catch (GuzzleHttp\Exception\ServerException $e) {
            $response = $e->getResponse();
            $result = $response->getBody()->getContents();
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            $result = $response->getBody()->getContents();
        }

        return $result;
    }
}
