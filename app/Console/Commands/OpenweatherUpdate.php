<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\UpdateOpenWeatherController;

class OpenweatherUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:update 
    {cityname? : Konkretne miasto}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aktulizacja prognozy podody wszytkich miast z BD';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(UpdateOpenWeatherController $uopc)
    {
        $uopc->updateWithApi($this->argument('cityname'));
    }
}
