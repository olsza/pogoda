<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenweathermapCity extends Model
{
    public function actualData()
    {
        return $this->hasMany('\App\OpenweathermapData', 'city_id')
        ->where('code', 200)
        ->orderBy('updated_at','DESC')
        ->first()
        ;
    }
}
