## Zadanie rekrutacyjne

Utworzenie prostą stronę główną, w której będzie pokazywała się dzisiejsza prognoza pogody np. dla Warszawy. Chodzi o wyświetlenie aktualnej temperatury w stopniach Celcjusza. Aktualną prognozę pogody pobieraj poprzez darmowe API - OpenWeatherMap.
 
Używając [Bootstrap](www.bootstrap.pl)-a ostylować tak, aby wyglądało czytelnie na mobile i desktopie. Moża dołożyć inne elementy informacyjne z pogodą, w formie boksów, np. wczorajsza wilgotność powietrza, statystyka w stosunku do poprzedniego tygodnia itd. Im lepiej to ostylowane i "zaprojektowane", tym lepiej.
  

###### Wskazówki:

 Aby uniknąć przekroczenia limitu na OpenWeatherMap warto zastosować jakiś cache, który w przypadku odświeżenia kilka razy strony, wykona request do API tylko raz na godzinę, a na stronie pokazać datę ostatniej aktualizacji temperatury.


# Rozwiązanie

~~Zadanie on-line mozna zobaczyć na (pogoda.czlowiek.it)~~
Strona główna wyświetla domyślnie dla miasta Warszawa, projekt jest przygotowany alternatywnie do wyświetlania innych miasta w przysłości, które będą podane w BD w tabeli "openweathermap_cities" gdzie dane miasto musi być wcześnie dodane z odpowiednim ID miasta odpwowiadjące w OpenWeatherMap, wtedy można w adresie podająć w adresie nazwę miasta np. /plock, /krakow itp.
Plik seed dla bazy zawiera TYLKO miasto Warszawa

Jeżeli jest ustawiony cron dla Laravel tj. schedule to dane z API OpenWeatherMap są aktulizowane co 1h (moża łatwo zmienić poprzez ustawienie stałej DEFAULT_TIME_CHECKING_MINUTES w klasie UpdateOpenWeatherController
W przypadku braku uruchomienia cron tj. schedule, można samemu ręcznie z aktulizować po przez polecenie w terminalu tj. `php artisan weather:update` jako parametr można podać miasto, które ma być aktulizowane, jeżeli mamy w BD więcej opróć Warszawy...
ww. komenta domyślnie aktulizuje "Warszawa"

###### Widok

Widok informuje dla jakiego miasta są wyświetlane dane oraz data ostatniej aktulizacji.
Dana są "aktualne" tj.zgodnie z ostatnią aktulizacją w BD

Na środku znajduję się temperatura oraz godzinę wschodu oraz zachodu słońca. (Box ten dla mobilnych będzie wyświetlany jako pierwszy)
Po lewej stronie od temperatujy znajduje się informacją o sile wiatru wraz z kierunkiem wiatru (strzałka), a po prawej stronie informacja jakie jest zachmurzenie wraz z nią ikonką

Animacja w tle "chmury" miały poruszać się w odpowiednią predkością wzależności od siły wiatru jaka jest, opcjonalnie miało być także w odpowiednim kierunku, a także tło miało być ciemniejsze lub jaśniejsze w zależności od pogody i jasności "słońca" - jest to możliwe, ale Córka za dnia życ nie dawała ;)
(jak widać po commitach GIT, większość godziny nocne) 