<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpenweathermapCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('openweathermap_cities', function (Blueprint $table) {
            $table->bigInteger('id')->unique();
            $table->char('name',50);
            $table->char('country',50)->default('PL');
            $table->float('coord_lon')->nullable();
            $table->float('coord_lat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('openweathermap_cities');
    }
}
