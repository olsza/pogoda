<?php

use Illuminate\Database\Seeder;

class Openweathermap_CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listCity = [
            'id' => 6695624,
            'name' => 'Warszawa',
            'coord_lon' => 21.04191,
            'coord_lat' => 52.23547,
        ];

        DB::table('openweathermap_cities')->insert($listCity);
    }
}
